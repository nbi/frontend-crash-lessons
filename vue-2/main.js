import Vue from 'vue';

new Vue({
    el: "#app",
    data: {
        titulo: 'Lista de tareas',
        nuevaTarea: '',
        tareas: [
            "Ir a Wallmart",
            "Comprar agua",
            "Comprar tenis",
        ]
    },
    methods: {
        agregarTarea(){
            if (!this.hasTarea) { return; }
            this.tareas.push(this.nuevaTarea);
            this.nuevaTarea='';
        },
        quitarTarea(tarea){
            this.tareas.splice(
                this.tareas.indexOf(tarea),
                1
            );
        }
    },
    computed: {
        hasTarea(){
            return this.nuevaTarea.length > 0;
        }
    }
});
