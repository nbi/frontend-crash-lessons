import Vue from 'vue';
import Input from './components/Input.vue';
import axios from 'axios';

new Vue({
    el:"#app",
    data: {
        users: [],
        api: { remote : 'https://jsonplaceholder.typicode.com' },
        form: {
            data: { nombre: '', apellido: '' },
            errors: {}
        }
    },
    methods: {
        getUsers(){

            axios.get(this.api.remote + "/users")
            .then((response) => {
                this.users = response.data;
            })
            .catch(function (error) {
                console.log(error);
            });

        }
    },
    components: { 'in': Input }
})
