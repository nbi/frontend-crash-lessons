import Vue from 'vue';
import Contador from './components/Contador.vue';

new Vue({
    el:"#app",
    data: {
        conteo: 0
    },
    components: {
        'contador': Contador
    }
})
