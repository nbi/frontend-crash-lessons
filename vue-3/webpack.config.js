module.exports = {
  entry: {
    main: './src/app.js',
    form: './src/formulario.js',
  },
  output: {
    filename: '[name].js',
    path: __dirname + '/public/js/'
  },
  module: {
    rules: [
    { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" },
    { test: /\.vue$/, loader: 'vue-loader' },
    { test: /\.css$/, use: [ 'css-loader' ] }
    ]
  },
  resolve: {
    alias: { vue: 'vue/dist/vue.js' }
  }
};
