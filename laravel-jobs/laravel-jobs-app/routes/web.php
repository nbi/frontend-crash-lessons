<?php

use Illuminate\Http\Request;
use App\Jobs\ReporteDiario;


Route::get('/', function () {
    return view('welcome');
});


Route::post('/mail', function(Request $request){

	info("Recibiendo datos", $request->toArray());

	$mensaje = $request->get('mensaje');
	$email = $request->get('email');

	dispatch(new ReporteDiario($email,$mensaje));

	

	return "Done";

});