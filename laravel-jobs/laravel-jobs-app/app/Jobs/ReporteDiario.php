<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class ReporteDiario implements ShouldQueue
{
    private $email;
    private $mensaje;

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $mensaje)
    {
        //
        $this->email = $email;
        $this->mensaje = $mensaje;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        info(__METHOD__);
        $email = $this->email;

        Mail::raw($this->mensaje, function ($message) use ($email) {
            $message->to($email);
        });

        
    }
}
