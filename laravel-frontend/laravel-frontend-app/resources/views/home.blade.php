@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <lista-usuarios
                titulo="Usuarios"
                sub="Estos usuarios estan activos en el sistema"
            ></lista-usuarios>
        </div>
    </div>
</div>
@endsection
