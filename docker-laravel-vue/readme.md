## Instalacion
* 2 Carpetas

## Primera carpeta: Clonar laradock
git clone https://github.com/Laradock/laradock.git

* renombrar laradock a -> docker-laravel-vue-server (o cualquiera que sea el nombre del proyecto)

## Segunda carpeta: Proyecto laravel

* `docker run --rm -v $(pwd):/app composer/composer create-project --prefer-dist laravel/laravel docker-laravel-vue-app`

## Permisos y propietario
* Cambiar propietario `sudo chown -R  charly:charly .`

## Servidor `docker-laravel-vue-server` crear archivo .env
Modifcar .env para instalar las librerias que se requieran en el proyecto, `.env` Describe lo que se va a instalar o no en el servidor o servidores, como librerias, binarios, utilerias.

## Levantar nginx
`docker-compose up -d nginx && docker-compose ps`

Para saber la ip del contenedor nginx

`docker inspect <nombre_del_contenedor_nginx> | grep IP`


## Entrar al contenedor workspace de mi proyecto desde el directorio del servidor

Entrar al contenedor workspace del servidor
`docker-compose exec workspace bash`

Crear el archivo .env de laravel desde el ejemplo y generar las claves para cifrado de la sesion
`cp .env.example .env && art key:generate`

> Ó un solo comando, desde la carpeta del servidor: `docker-compose exec workspace cp .env.example .env && art key:generate`

Dentro de la carpeta de `docker-laravel-vue-app` ejecutar `npm install && npm run watch` para descargar las librerias de node y poder trabajar con vue y compilar

Para inicializar la autenticacion en laravel corremos `php artisan make:auth`

Para inicializar la base de datos con sqlite `touch database/database.sqlite`

Correr las migraciones (workspace) `art migrate`

Registrar un usuario de prueba.
