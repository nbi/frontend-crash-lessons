<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTareasTable extends Migration
{
    public function down()
    {
        Schema::dropIfExists('tareas');
    }

    public function up()
    {
        Schema::create('tareas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->boolean('terminada')->default(0);
            $table->timestamps();
        });
    }
}
