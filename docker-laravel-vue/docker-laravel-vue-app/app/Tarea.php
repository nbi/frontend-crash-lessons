<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'nombre',
        'terminada',
    ];
}
