<?php

Route::group([
    'prefix' => 'tarea',
    'as'     => 'api::tarea',
], function ($tarea) {

    $tarea->get('/', function (Illuminate\Http\Request $request) {
        return App\Tarea::orderBy('created_at', 'DESC')->paginate($request->get('cuantas'));
    })->name('.index');

    $tarea->post('/', function (Illuminate\Http\Request $request) {
        return App\Tarea::create([
            'nombre' => $request->get('tarea'),
        ]);
    })->name('.store');

});

Route::group([
    'prefix' => 'usuario',
    'as'     => 'api::usuario',
], function ($usuarios) {

    $usuarios->get('/', function (Illuminate\Http\Request $request) {
        return App\User::paginate($request->get('cuantas'));
    })->name('.index');

});
