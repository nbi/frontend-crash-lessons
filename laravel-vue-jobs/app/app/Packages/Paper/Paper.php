<?php

namespace App\Packages\Paper;

use setasign\Fpdi\Fpdi;

class Paper
{
    /**
     * @var mixed
     */
    protected $count;

    /**
     * @var mixed
     */
    protected $filename;

    /**
     * @var mixed
     */
    protected $instance;

    /**
     * @var mixed
     */
    protected $pdf;

    /**
     * @param $filename
     * @param $pdf
     * @param $count
     */
    public function __construct($pdf, $filename = null)
    {
        $this->filename = $filename;
        $this->pdf      = $pdf;
        $this->count    = $this->filename ? $this->pdf->setSourceFile($this->filename) : 0;

    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        if (property_exists($this, $name)) {
            return $this->{$name};
        }

        throw new \Exception("Propiedad {$name} no encontrada.");
    }

    /**
     * @param $image
     * @param $x
     * @param $y
     * @param $save_path
     */
    public function addImageOverlay($image, $x, $y, $w, $h, $save_path)
    {
        // info(__METHOD__ . "| Adding Overlay {$image}");
        $newpdf = new Fpdi();
        $newpdf->setSourceFile($this->filename);
        if (!$tplIdx = $newpdf->importPage('1')) {
            throw new Exception(
                "No se pudo cargar pagina '$page' de '{$this->filename}'."
            );
        }
        $size = $newpdf->getTemplateSize($tplIdx);
        $newpdf->AddPage('P', array($size['width'], $size['height']));
        $newpdf->useTemplate($tplIdx);
        $newpdf->Image($image, $x, $y, $w, $h, 'PNG', 'www.nbintelligence.com');

        if (!is_dir(dirname($save_path))) {
            mkdir(dirname($save_path), 0777, true);
        }

        return ($newpdf->Output($save_path, 'F') == '') ? $save_path : false;

    }

    /**
     * @param $page
     * @param $save_path
     * @return mixed
     */
    public function extractPage($page, $save_path)
    {
        $newpdf = new Fpdi();
        $newpdf->setSourceFile($this->filename);
        if (!$tplIdx = $newpdf->importPage($page)) {
            throw new Exception(
                "No se pudo cargar pagina '$page' de '{$this->filename}'."
            );
        }
        $size = $newpdf->getTemplateSize($tplIdx);
        $newpdf->AddPage('P', array($size['width'], $size['height']));
        $newpdf->useTemplate($tplIdx);

        if (!is_dir(dirname($save_path))) {
            mkdir(dirname($save_path), 0777, true);
        }

        return ($newpdf->Output($save_path, 'F') == '') ? $save_path : false;
    }

    /**
     * @param $filename
     */
    public static function load($filename)
    {
        return new static(new Fpdi, $filename);
    }
}
