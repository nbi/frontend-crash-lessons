<?php
namespace App\Packages\Paper\Templates;

use Milon\Barcode\DNS1D;
use Milon\Barcode\DNS2D;
use App\Packages\Paper\Paper;
use Intervention\Image\Facades\Image;

class ArribaDerecha
{
    /**
     * Remote Order
     * @var mixed
     */
    protected $model;

    /**
     * @var int
     */
    /**
     * @param $container
     * @param $element
     */
    protected $px_unit = 37;

    /**
     * @param $container
     * @param $element
     */
    public function centerOn($container, $element)
    {
        return floor(($container / 2) - ($element / 2));
    }

    public function handle()
    {
        // info(__METHOD__ . "|Stamp|{$this->model->uuid}|{$this->model->pdf}");
        $w       = 13;
        $h       = 7.5;
        $quality = 3;
        $image   = Image::canvas($this->px_unit * $w, $this->px_unit * $h);
        $barcode = $this->resizeImage(
            DNS1D::getBarcodePNG($this->model->guia, "C39", 2, 60),
            floor(.95 * $w * $this->px_unit)
        );
        $image->insert(
            $barcode,
            'bottom-left',
            5,
            7
        );

        $qrcode = DNS2D::getBarcodePNG(
            $this->model->guia,
            "QRCODE,H",
            6,
            7
        );
        $image->insert(
            $qrcode,
            'top-left',
            5,
            5
        );
        $image->text(
            $this->model->guia,
            6,
            189,
            function ($font) {
                $font->file(public_path('fonts/WorkSans-Light.ttf'));
                $font->size(23);
                $font->color('#000');
                $font->align('left');
                $font->valign('top');
            }
        );

        $imagePath = storage_path(
            "app/stamps/{$this->model->file->uuid}/{$this->model->uuid}.png"
        );
        if (!is_dir(dirname($imagePath))) {
            mkdir(dirname($imagePath), 0777, true);
        }
        $image->save($imagePath, $quality);
        $this->model->image = $imagePath;
        $this->model->save();

        $pdf       = Paper::load($this->model->pdf);
        $stampPath = storage_path(
            "app/stamps/{$this->model->file->uuid}/{$this->model->uuid}.pdf"
        );
        $pdf->addImageOverlay($imagePath, 121, 11, 67, 34, $stampPath);
        $this->model->stamp = $stampPath;
        $this->model->save();
    }

    /**
     * @param $model
     */
    /**
     * @param $source
     * @param $w
     * @param null $h
     */
    public function model($model)
    {
        $this->model = $model;
    }

    /**
     * @param $source
     * @param $w
     * @param null $h
     */
    public function resizeImage($source, $w = null, $h = null)
    {
        return Image::make($source)->resize(
            $w,
            $h,
            function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            }
        );
    }
}
