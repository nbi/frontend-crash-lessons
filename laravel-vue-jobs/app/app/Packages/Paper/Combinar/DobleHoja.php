<?php

namespace App\Packages\Paper\Combinar;

use App\Packages\Paper\PaperMerge;

class DobleHoja
{
    /**
     * @var mixed
     */
    protected $model;

    /**
     * summary
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    public function handle()
    {
        $merge = new PaperMerge;
        foreach ($this->model->orders as $order) {
            $merge
                ->add($order->stamp, '1')
                ->add($order->pdf, '1');
        }
        $stampPath = storage_path(
            "app/stamps/{$this->model->uuid}.pdf"
        );
        if (!is_dir(dirname($stampPath))) {
            mkdir(dirname($stampPath), 0777, true);
        }
        $merge->save($stampPath);
        $this->model->stamp = $stampPath;
        $this->model->save();
        info(__METHOD__ . "|Merged {$stampPath}");
    }
}
