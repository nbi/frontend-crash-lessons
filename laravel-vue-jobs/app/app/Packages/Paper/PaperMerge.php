<?php

namespace App\Packages\Paper;

use setasign\Fpdi\Fpdi;

class PaperMerge
{
    /**
     * @var mixed
     */
    protected $files;

    public function __construct()
    {
        $this->files = [];
    }

    /**
     * @param $file
     * @param $page
     */
    public function add($file, $page)
    {
        if (!file_exists($file)) {
            throw new \Exception("Could not locate PDF on '{$file}'");
        }
        array_push($this->files, [
            'path' => $file,
            'page' => $page,
        ]);

        return $this;
    }

    /**
     * @param $path
     */
    public function save($path)
    {
        if (!count($this->files)) {
            throw new \Exception("No PDFs to merge.");
        }

        $pdf = new Fpdi();

        foreach ($this->files as $file) {
            $name = $file['path'];
            $page = $file['page'];
            $pdf->setSourceFile($name);
            if (!$template = $pdf->importPage($page)) {
                throw new \Exception(
                    "Could not load page '$page' in PDF '$name'. Check that the page exists."
                );
            }
            $size = $pdf->getTemplateSize($template);
            $pdf->AddPage('P', array($size['width'], $size['height']));
            $pdf->useTemplate($template);
        }

        if (!is_dir(dirname($path))) {
            mkdir(dirname($path), 0777, true);
        }

        return ($pdf->Output($path, 'F') == '') ? $path : false;
    }
}
