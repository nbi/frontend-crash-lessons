<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archivo extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'ruta',
        'nombre',
        'ruta_parcial',
        'pagina',
    ];

    /**
     * @param $request
     */
    public function toArray()
    {
        return [
            'id'             => $this->id,
            'nombre'         => $this->nombre,
            'parcial'        => $this->ruta_parcial,
            'url'            => route('api::archivo.download', ['id' => $this->id]),
            'parcial_pagina' => $this->pagina,
        ];

    }
}
