<?php

namespace App\Http\Controllers;

use App\Archivo;
use Carbon\Carbon;
use App\Jobs\SplitArchivo;
use Illuminate\Http\Request;
use App\Http\Requests\PdfRequest;
use App\Http\Resources\ArchivoResource;

class ArchivoController extends Controller
{
    /**
     * @param Archivo $archivo
     * @return mixed
     */
    public function destroy(Archivo $archivo)
    {
        return $archivo->delete();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function download($id)
    {
        return response()->download(Archivo::find($id)->ruta_parcial);
    }

    public function index()
    {
        return new ArchivoResource(Archivo::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Archivo  $archivo
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        return new ArchivoResource(Archivo::find($id));
    }

    /**
     * @param PdfRequest $request
     */
    public function store(PdfRequest $request)
    {
        $pdf      = $request->file('pdf');
        $pagina   = $request->get('pagina');
        $date     = Carbon::now('America/Mexico_City')->format("YMDHis");
        $filename = "Original_$date.pdf";

        $path    = $request->pdf->storeAs('uploads', $filename);
        $archivo = Archivo::create([
            'ruta'   => $path,
            'nombre' => $pdf->getClientOriginalName(),
            'pagina' => $pagina,
        ]);

        dispatch(new SplitArchivo($archivo));

        return new ArchivoResource($archivo);
    }
}
