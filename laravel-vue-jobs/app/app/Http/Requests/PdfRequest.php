<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PdfRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'pagina.required' => 'El numero de pagina es obligatorio.',
            'pdf.mimes'       => 'El formato del archivo no es valido.',
            'pdf.file'        => 'El archivo no cargo correctamente.',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pdf'    => 'file|mimes:pdf',
            'pagina' => 'required',
        ];
    }
}
