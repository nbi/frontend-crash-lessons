<?php

namespace App\Jobs;

use Carbon\Carbon;
use App\Packages\Paper\Paper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SplitArchivo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var mixed
     */
    protected $archivo;

    /**
     * @param $archivo
     */
    public function __construct($archivo)
    {

        $this->archivo = $archivo;
    }

    public function handle()
    {
        $pdf          = Paper::load(storage_path('app') . "/" . $this->archivo->ruta);
        $date         = Carbon::now('America/Mexico_City')->format("YMDHis");
        $outfilename  = "{$this->archivo->id}_{$date}.pdf";
        $parcial_ruta = $pdf->extractPage(
            $this->archivo->pagina,
            storage_path('parciales') . "/{$outfilename}"
        );
        $this->archivo->ruta_parcial = ($parcial_ruta) ?: null;
        $this->archivo->save();

        info(__METHOD__ . " | creado {$this->archivo->ruta_parcial}");

    }
}
