<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'archivo',
    'as'     => 'api::archivo',
], function ($archivo) {

    $archivo->get('/', [
        'as'   => '.index',
        'uses' => 'ArchivoController@index',
    ]);

    $archivo->get('/{id}', [
        'as'   => '.show',
        'uses' => 'ArchivoController@show',
    ]);

    $archivo->get('/{id}/download', [
        'as'   => '.download',
        'uses' => 'ArchivoController@download',
    ]);

    $archivo->post('/', [
        'as'   => '.store',
        'uses' => 'ArchivoController@store',
    ]);

});
